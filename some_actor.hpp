#ifndef MY_MQ_AACT
#define MY_MQ_AACT

#include "lib/message/message.hpp"
#include "lib/actor/actor.hpp"

class SomeActor : public Actor {
public:
    SomeActor() : Actor("tipo1") { }
    
    void onReceive(Message &message);
    
    void onFinish(Message &message);
};

#endif