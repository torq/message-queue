#include <iostream>

#include "some_actor.hpp"

void SomeActor::onReceive(Message &message) {
	std::cout << ":: Processing the message:\n";
    std::cout << "\t" << message.getUuid() << " - " << message.getType() << std::endl;
}

void SomeActor::onFinish(Message &message) {
	std::cout << ":: Message processed." << std::endl;
}