#include <iostream>
#include <string>

#include "lib/message_queue.hpp"
#include "lib/message/message.hpp"
#include "some_actor.hpp"

void send(std::string type, MessageQueue &queue) {
	Message message(type);
	queue.receive(message);
}

// --------- MAIN ---------
int main(int argc, char** args) {
	std::cout << "Initiating system..." << std::endl;
	MessageQueue queue;

	std::cout << "Subscribing..." << std::endl;
	SomeActor actor1;
	queue.subscribe(&actor1);
	
	std::cout << "Sending messages..." << std::endl;
	send("tipo1", queue);
	send("tipo2", queue);

	std::cout << "Unsubscribing..." << std::endl;
	queue.unsubscribe(&actor1);

	std::cout << "Done." << std::endl;
	return 0;
}