#ifndef MY_MQ_UUID
#define MY_MQ_UUID

#include <string>

class Uuid {
public:
    static std::string generate();

private:
    static std::string formatNumber(unsigned number);
};

#endif