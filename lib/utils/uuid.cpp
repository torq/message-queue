#include <random>
#include <string>
#include <sstream>

#include "uuid.hpp"

std::string Uuid::generate() {
    std::random_device rd;
    return Uuid::formatNumber(rd());
}

std::string Uuid::formatNumber(unsigned number) {
    std::string result = static_cast<std::ostringstream*>( &(std::ostringstream() << number) )->str();
    return result;
}