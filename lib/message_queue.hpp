#ifndef MY_MQ_MQ
#define MY_MQ_MQ

#include <string>
#include <deque>
#include <map>
#include <queue>

#include "message/message.hpp"
#include "actor/actor.hpp"

class MessageQueue {
public:
	void subscribe(Actor *actor);
    
    void unsubscribe(std::string type, std::string uuid);

	void unsubscribe(Actor *actor);

    void receive(Message message);

private:
	std::map<std::string, std::deque<Message*>> messageMap;
	std::map<std::string, std::deque<Actor*>> actorMap;

	void store(Message *message);
};

#endif