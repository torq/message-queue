#include <iostream>
#include <stdexcept>

#include "message_queue.hpp"

void MessageQueue::subscribe(Actor *actor) {
	auto type = actor->getType();
	if (type.empty())
		return;

	this->actorMap[type].push_back(actor);
}

void MessageQueue::unsubscribe(std::string type, std::string uuid) {
	if (type.empty() || uuid.empty())
		return;

    try {
		auto actorDeque = this->actorMap.at(type);

		auto it = actorDeque.begin();
		for (; it != actorDeque.end(); ++it)
			if ((*it)->getUuid() == uuid)
				break;

        if(it != actorDeque.end())
		    this->actorMap.at(type).erase(it);
	} catch (const std::out_of_range oor) {
		return;
	}
}

void MessageQueue::unsubscribe(Actor *actor) {
	this->unsubscribe(actor->getType(), actor->getUuid());
}

void MessageQueue::receive(Message message) {
	auto type = message.getType();
	if (type.empty())
		return;

	try {
		auto actorDeque = this->actorMap.at(type);
		if(actorDeque.empty())
			throw std::exception();
		
		auto actor = actorDeque.front();
		actorDeque.pop_front();
		actor->process(message);
		actorDeque.push_front(actor);
	} catch (const std::out_of_range& oor) {
		std::cout << ":: ERR: OOR" << std::endl;
		this->store(&message);
		return;
	} catch (const std::invalid_argument& ia) {
		std::cout << ":: ERR: IA" << std::endl;
		this->store(&message);
		return;
	} catch (const std::exception& e) {
		std::cout << ":: ERR: E" << std::endl;
		this->store(&message);
		return;
	}
}

void MessageQueue::store(Message* message) {
	const auto type = message->getType();
	if(type.empty())
		return;

    this->messageMap[type].push_back(message);
}