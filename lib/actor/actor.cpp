#include <stdexcept>

#include "actor.hpp"

void Actor::process(Message &message) {
	if(message.getType() != this->getType())
		throw std::invalid_argument("This message is not my type ");

	this->onReceive(message);
	this->onFinish(message);
}