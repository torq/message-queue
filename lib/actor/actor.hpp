#ifndef MY_MQ_ACT
#define MY_MQ_ACT

#include <string>

#include "../message/message.hpp"
#include "../utils/uuid.hpp"

class Actor {
public:
	Actor(std::string type)
	: _type(type)
	, _uuid(Uuid::generate()) { }

	// getters & setters
	std::string getUuid() { return this->_uuid; }
	std::string getType() { return this->_type; }

	/**
     * Process the message according to its lifecycle.
	 * onReceive -> onFinish.
    */
    void process(Message &message) ;

	/**
     * Callback to desbribe what needs to be done when receiving the message.
     * Here, you are supposed to read the message and process it.
    */
    virtual void onReceive(Message &message) = 0;

	/**
     * Callback to desbribe what needs to be done after receiving the message.
     * Here, you are supposed to answer the message.
    */
	virtual void onFinish(Message &message) = 0;

private:
	std::string _uuid;
	std::string _type;
};

#endif