#ifndef MY_MQ_MSG
#define MY_MQ_MSG

#include <string>
#include "../utils/uuid.hpp"

class Message {
public:
	Message(std::string type)
	: _type(type)
	, _uuid(Uuid::generate()) { }

	// getters & setters
	std::string getUuid() { return this->_uuid; }
	std::string getType() { return this->_type; }
	
private:
	std::string _uuid;
	std::string _type;
};

#endif